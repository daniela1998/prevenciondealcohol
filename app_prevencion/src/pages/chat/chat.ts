
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SesionProvider } from '../../providers/sesion/sesion';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  formulario: FormData = new FormData();
  chats: any[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public sesion: SesionProvider) {
    this.chats[0] = {
      nombre: "Adrian Rojas"
    };
    this.chats[1] = {
      nombre: "Nerea Hernandez Gonzalez"
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

  ngOnInit() {
    // Si se carga el perfil habilitar el menu lateral
    
    console.log("ionOnInit RecetasPage");
  }
}
