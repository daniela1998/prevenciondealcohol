import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SesionProvider } from "../../providers/sesion/sesion";
import { LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the DetalleChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-chat',
  templateUrl: 'detalle-chat.html',
})
export class DetalleChatPage {
  nmensaje: number
  mensajes: any[] = [];
  loading: any;
  mensaje:string;
  respuestas: string[] = [];
  respuesta: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sesion: SesionProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {
      this.nmensaje = 0;
      this.mensaje = "";
      this.respuesta = "";
      this.respuestas[0] = "Hola, ¿en qué puedo ayudarte?";
      this.respuestas[1] = "Lo siento no te he entendido";
      this.respuestas[2] = "Mi número de contacto es 231-206-5506";
      this.respuestas[3] = "Si lo necesitas puedes tomar un cita aquí en DIF en los horarios de 8:00 a 13:00";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleChatPage');
  }
  presentarLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Enviando...'
    });
    this.loading.present();
  }
  presentarAlerta(mensaje: string){
    let alert = this.alertCtrl.create({
      title: 'La aplicación dice...',
      subTitle: mensaje,
      buttons: ['Ok']
    });
    alert.present();
  }
  enviar(){
    if(this.mensaje != ""){
      let mess = this.mensaje;
      let idu = this.sesion.iduser;
      this.mensajes[this.nmensaje] = {mess, idu};
      this.nmensaje++;
      if(this.nmensaje == 1){
        mess = this.respuestas[0];
        idu = 2;
        this.mensajes[this.nmensaje] = {mess, idu};
        this.nmensaje++;
      }
      console.log(JSON.stringify(this.mensajes));
      this.ionViewDidLoad();
    }
    else{
      this.presentarAlerta("No se puede envíar un mensaje en blanco");
    }
  }
}
