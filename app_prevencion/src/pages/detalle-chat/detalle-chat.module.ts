import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleChatPage } from './detalle-chat';

@NgModule({
  declarations: [
    DetalleChatPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleChatPage),
  ],
})
export class DetalleChatPageModule {}
