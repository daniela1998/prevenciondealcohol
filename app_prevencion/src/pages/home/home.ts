import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { SesionProvider } from '../../providers/sesion/sesion';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  splash = true;
  inicioPage ="PrincipalPage";
  usuario: string = "";
  contra: string = "";
  constructor(public navCtrl: NavController, public sesion: SesionProvider, public toastCtrl: ToastController) {
    this.usuario = '';
    this.contra = '';
  }
  ionViewDidLoad() {
    setTimeout(() => {
      this.splash = false;
    }, 5000);
  }

  
  loggearse(){
    if(this.sesion.validarSesion(this.usuario, this.contra)){
      this.navCtrl.push("PrincipalPage");
    }else{
      this.presentToast("Usuario y/o contraseña incorrectos");
    }

  }  
  presentToast(mensaje: string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000
    });
    toast.present();
  }
}
