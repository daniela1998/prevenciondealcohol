import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
/*
  Generated class for the MetodosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MetodosProvider {
  dominio: string;
  constructor(public http2: Http) {
    console.log('Hello MetodosProvider Provider');
    this.dominio = 'http://192.168.0.101:90/AlcoholApp/ServiciosWeb/';
  }

  public getChats(data: FormData){
    return new Promise((resolve, reject) => {
      this.http2.post(this.dominio+'recuperarChats.php', data).subscribe(resp => {
        resolve(resp.json());
      }, err =>{
        reject(JSON.stringify(err));
      });
    });
  }

  public getMensajes(data: FormData){
    return new Promise((resolve, reject) => {
      this.http2.post(this.dominio+'recuperarMensajes.php', data).subscribe(resp => {
        resolve(resp.json());
      }, err =>{
        reject(JSON.stringify(err));
      });
    });
  }

  public addMensaje(data: FormData){
    return new Promise((resolve, reject) => {
      this.http2.post(this.dominio+'insertarMensaje.php', data).subscribe(resp => {
        resolve(resp.json());
      }, err =>{
        reject(JSON.stringify(err));
      });
    });
  }
}
